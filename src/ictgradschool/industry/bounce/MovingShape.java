package ictgradschool.industry.bounce;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MovingShape extends Shape {
    private Image picture;

    public MovingShape() {
        super();
        loadImage();
    }

    public MovingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
        loadImage();
    }

    public MovingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
        loadImage();
    }

    @Override
    public void paint(Painter painter) {
        painter.fillImage(picture, getX(), getY());
    }

    public void loadImage() {
        String imagePath = "circlepicture.png";
        BufferedImage myPicture = null;

        {
            try {
                myPicture = ImageIO.read(new File(imagePath));
            } catch (IOException e) {
                e.printStackTrace();
            }

            picture = myPicture.getScaledInstance(getWidth(), getHeight(), Image.SCALE_DEFAULT);
        }
    }
}
