package ictgradschool.industry.bounce;

import java.awt.*;

public class DynamicRectangle extends Shape {

    Boolean colorChange = false;

    public DynamicRectangle() {
        super();
    }

    public DynamicRectangle(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public DynamicRectangle(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void move(int width, int height) {
        int previousDeltaX = fDeltaX;
        int previousDeltaY = fDeltaY;
        super.move(width, height);
        if (previousDeltaX == -fDeltaX) {
            colorChange = true;
        } else if (previousDeltaY == -fDeltaY){
            colorChange = false;
        }
    }

    @Override
    public void paint(Painter painter) {
        {
            if(colorChange == false) {
                painter.drawRect(fX, fY, fWidth, fHeight);
            }else if(colorChange == true) {
                painter.setColor(Color.CYAN);
                painter.fillRect(fX, fY, fWidth, fHeight);
            }
        }
    }
}