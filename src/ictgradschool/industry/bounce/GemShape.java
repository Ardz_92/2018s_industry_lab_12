package ictgradschool.industry.bounce;

import java.awt.*;

public class GemShape extends Shape {

    public GemShape() {
        super();
    }

    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }


    @Override
    public void paint(Painter painter) {
        if (getWidth() <= 40) {
            Polygon smallpolygon = new Polygon(new int[]{getX(), getX() + getWidth() / 2, getX() + getWidth(), getX() + getWidth() / 2},
                    new int[]{getY() + getHeight() / 2, getY(), getY() + getHeight() / 2, getY() + getHeight()}, 4);
            painter.drawPolygon(smallpolygon);
        } else if (getWidth() > 40) {
            Polygon bigpolygon = new Polygon(new int[]{getX(), getX() + 20, getX() + getWidth() - 20, getX() + getWidth(), getX() + getWidth() - 20, getX() + 20},
                    new int[]{getY() + getHeight() / 2, getY(), getY(), getY() + getHeight() / 2, getY() + getHeight() , getY() + getHeight()}, 6);
            painter.drawPolygon(bigpolygon);
        }
    }
}